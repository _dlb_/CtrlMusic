//
//  ViewController.swift
//  CrtlMusic
//
//  Created by Leandro Brum on 26/08/2018.
//  Copyright © 2018 Leandro Brum. All rights reserved.
//

import UIKit
import FirebaseAuth

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let autenticacao = Auth.auth()
        
        
        do {
         try autenticacao.signOut()
         } catch {
         print("Erro ao deslogar usuario")
        }
        
        autenticacao.addStateDidChangeListener { (autenticacao, usuario) in
            
            if usuario != nil {
                self.performSegue(withIdentifier: "loginAutomaticoSegue", sender: nil)
            }
            
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

