//
//  ViewControleInicial.swift
//  CrtlMusic
//
//  Created by Leandro Brum on 02/10/2018.
//  Copyright © 2018 Leandro Brum. All rights reserved.
//

import Foundation
import UIKit

class ViewControllerInicial: UITableViewController{
    
    var dados: [String] = ["Lazanha","Pizza","Torta"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dados.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celulaReuso = "celulaReuso"
        let celula = tableView.dequeueReusableCell(withIdentifier: celulaReuso, for: indexPath)
        celula.textLabel?.text = dados[ indexPath.row ]
        return celula
    }
}
