//
//  cadastraMusicaViewController.swift
//  CtrlMusic
//
//  Created by Leandro Brum on 18/12/18.
//  Copyright © 2018 Leandro Brum. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase



class cadastraMusicaViewController: UIViewController {

    @IBOutlet weak var nome: UITextField!

    @IBOutlet weak var arquivo: UITextField!
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    
    
    func exibirMensagem(titulo: String, mensagem: String){
        
        let alerta = UIAlertController(title: titulo, message: mensagem, preferredStyle: .alert)
        let acaoCancelar = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        alerta.addAction( acaoCancelar )
        present(alerta, animated: true, completion: nil)
        
    }
    
    @IBAction func cadMusica(_ sender: Any) {
        
  if let nomeR = self.nome.text {
    if let arquivoR = self.arquivo.text {
        
        
        if nomeR != "" {
            if arquivoR != "" {
                
                let database = Database.database().reference()
                let musica = database.child("musica")
                let musicaDados = ["nome": nomeR,"arquivo":arquivoR]
                
                
                musica.child(nomeR).setValue(musicaDados)
                
                                 self.performSegue(withIdentifier: "musicaloginsegue", sender: nil)
                
            }// se arquivo é diferente de vazio
            else{
                self.exibirMensagem(titulo: "Dados incompletos.", mensagem: "O Arquivo deve ser preenchido.")
            }
            
        }else{
                self.exibirMensagem(titulo: "Dados incompletos.", mensagem: "O nome deve ser preenchido.")
            }
            
            
    
    }// se nome é diferente de vazio ele executa
    
        }// arquivoR recebe o valor do campo arquivo na tela
       // nomeR recebe valor do campo nome na tela
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
