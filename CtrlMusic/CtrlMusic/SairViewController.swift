//
//  SairViewController.swift
//  CtrlMusic
//
//  Created by Leandro Brum on 19/10/2018.
//  Copyright © 2018 Leandro Brum. All rights reserved.
//

import UIKit
import FirebaseAuth

class SairViewController: UIViewController {

    
    @IBAction func sair(_ sender: Any) {
        let autenticao = Auth.auth()
        do {
            try autenticao.signOut()
        } catch {
            print ("Erro ao deslogar")
        
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
