//
//  ViewController.swift
//  DropDown
//
//  Created by SHUBHAM AGARWAL on 30/03/18.
//  Copyright © 2018 SHUBHAM AGARWAL. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth


class ProductionViewController: UIViewController , UITableViewDataSource , UITableViewDelegate {
    
    @IBOutlet weak var btnDrop: UIButton!
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var btnMusDrop: UIButton!
    @IBOutlet weak var tblMusView: UITableView!
    
    @IBOutlet weak var btnEstDrop: UIButton!
    @IBOutlet weak var tblEstView: UITableView!
    
    var refEstudio: DatabaseReference!
    
    var produtorList = ["d!b", "outro", "qualquer", "fraco"]
    
    var estudioList = [EstudioModel]()
    
    //var estudioList = ["d!b_HomeStudio","Concorrente A","Concorrente B"]
    var musicaList = ["Caminhada","Sonhar","Skt"]
    
    let cellIdPrd: String = "cell"
    let cellIdMus: String = "cellMus"
    let cellIdEst: String = "cellEst"

    
    

    var numeroItens : Int = 0

    
     let firebase = Database.database().reference()
    
    override func viewDidLoad() {
        super.viewDidLoad()
      //  FirebaseApp.configure()
        
        tblView.isHidden = true
        tblMusView.isHidden = true
        tblEstView.isHidden = true
        // Do any additional setup after loading the view, typically from a nib.
    
        tblView.dataSource = self
        tblView.delegate = self
        tblView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    
        tblMusView.dataSource = self
        tblMusView.delegate = self
        tblMusView.register(UITableViewCell.self, forCellReuseIdentifier: "cellMus")
    
        tblEstView.dataSource = self
        tblEstView.delegate = self
        tblEstView.register(UITableViewCell.self, forCellReuseIdentifier: "cellEst")
  
        //variavel que recebe a tabela do banco de dados firebase
        refEstudio = Database.database().reference().child("estudio");
        
        //observador de mudanca dos dados no banco
        refEstudio.observe(DataEventType.value, with: { (snapshot) in
            
            //if the reference have some values
            if snapshot.childrenCount > 0 {
                
                //clearing the list
               // self.estudioList.removeAll()
                
                //iterating through all the values
                for estudios in snapshot.children.allObjects as! [DataSnapshot] {
                    //getting values
                    let estudioObject = estudios.value as? [String: AnyObject]
                    let estudioEmail  = estudioObject?["email"]
                    let estudioNome  = estudioObject?["nome"]
                    let estudioFone = estudioObject?["telefone"]
                    
                    //creating artist object with model and fetched values
                    let estudio = EstudioModel(email: estudioEmail as! String?, nome: estudioNome as! String?, telefone: estudioFone as! String?)
                    
                    //appending it to list
                    self.estudioList.append(estudio)
                }
                
                //reloading the tableview
                self.tblEstView.reloadData()
            }
        })
        
     
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickDropButton(_ sender: Any) {
        if tblView.isHidden {
            animate(toogle: true, type: btnDrop)
        } else {
            animate(toogle: false, type: btnDrop)
        }
        
    }
    
    
    
    @IBAction func onClickDropButtonMus(_ sender: Any) {
        
        if tblMusView.isHidden {
            animate(toogle: true, type: btnMusDrop)
        } else {
            animate(toogle: false, type: btnMusDrop)
        }
    }
    
    @IBAction func onClickDropButtonEst(_ sender: Any) {
        
        if tblEstView.isHidden {
            animate(toogle: true, type: btnEstDrop)
        } else {
            animate(toogle: false, type: btnEstDrop)
        }
        
    }
    
    
    

    func animate(toogle: Bool, type: UIButton) {
        // função para abrir o botão e mostrar a tableview
       
       
        if type == btnDrop {
     
            if toogle {
                UIView.animate(withDuration: 0.3) {
                    self.tblView.isHidden = false
                  
                }
            } else {
                UIView.animate(withDuration: 0.3) {
                    self.tblView.isHidden = true
                }
            }
        }
       
        else
        //fim do drop do produtor
        
        if type == btnMusDrop {
            
            if toogle {
                UIView.animate(withDuration: 0.3) {
                    self.tblMusView.isHidden = false
            
                }
            } else {
                UIView.animate(withDuration: 0.3) {
                    self.tblMusView.isHidden = true
                }
            }
    }
         //fim do drop

        if type == btnEstDrop {
            
            if toogle {
                UIView.animate(withDuration: 0.3) {
                    
                    //self.firebase.child("estudio").child("nome")
                    self.tblEstView.isHidden = false
    
                }
            } else {
                UIView.animate(withDuration: 0.3) {
                    self.tblEstView.isHidden = true
                }
            }
        }//fim do drop
   
   
        
    }
    /* função para adicionar elementos da tela
    func addArtist(){
        //generating a new key inside artists node
        //and also getting the generated key
        let key = refArtists.childByAutoId().key
        
        //creating artist with the given values
        let artist = ["id":key,
                      "artistName": textFieldName.text! as String,
                      "artistGenre": textFieldGenre.text! as String
        ]
        
        //adding the artist inside the generated unique key
        refArtists.child(key).setValue(artist)
        
        //displaying message
        labelMessage.text = "Artist Added"
    }
    */
    
}





extension ProductionViewController {

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
   
        // função que retorna a quantidade de itens nas variáveis para exibir na tableview
        if (tableView.tag == 1 )
        {
             numeroItens = produtorList.count
             return numeroItens
            
        }
      
        else if (tableView.tag == 2)
        {
            numeroItens = musicaList.count
            
        }
        else if (tableView.tag == 3)
        {
            //dbestudios = firebase.child("estudio").child("nome")
          numeroItens = estudioList.count
            
        }
   
        return numeroItens
        }
    
    


    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //função que exibe na tableview o que esta na váriavel
        
        let cellTemp = UITableViewCell()
        
        if (tableView.tag == 1 )
        {
         
            let cellProd = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            cellProd.textLabel?.text = produtorList[indexPath.row]
            
            return cellProd
            
        }
        else if (tableView.tag == 2 )
        {
            let cellMus = tableView.dequeueReusableCell(withIdentifier: "cellMus", for: indexPath)
            cellMus.textLabel?.text = musicaList[indexPath.row]
           return cellMus
        
        }
       else if (tableView.tag == 3 )
        {
            let cellEst = tableView.dequeueReusableCell(withIdentifier: "cellEst", for: indexPath)
           // cellEst.textLabel?.text = estudioList[indexPath.row]
           let estudio: EstudioModel
            
            //recebendo o estudio da posicao selecionada
            estudio = estudioList[indexPath.row]
            cellEst.textLabel?.text = estudio.nome
            
            //adicionando valores
            /*
            cellEst.labelName.text = estudio.nome
            */
            return cellEst
        }
 
 
        return cellTemp

        //Users/leandrobrum/Downloads/iOS
            //let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
           // cell.textLabel?.text = produtorList[indexPath.row]
           // return cell
    
 
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // função que exibe o selecionado no botão o que esta na tableview
       
        if (tableView.tag == 1 )
        {
        //if (btnDrop == self.btnDrop)
        if(btnDrop?.isUserInteractionEnabled == true)
        {
            btnDrop.setTitle("\(produtorList[indexPath.row])", for: .normal)
            animate(toogle: false, type: btnDrop)
            }}
        
        if (tableView.tag == 2 )
        {
        //if (btnMusDrop == self.btnMusDrop)
        if(btnMusDrop?.isUserInteractionEnabled == true)
        {
            btnMusDrop.setTitle("\(musicaList[indexPath.row])", for: .normal)
            animate(toogle: false, type: btnMusDrop)
            }}
        
        if (tableView.tag == 3 )
        {
        if(btnEstDrop?.isUserInteractionEnabled == true)
       // if (btnEstDrop == self.btnEstDrop )
        {
            
            let estudio: EstudioModel
            estudio = estudioList[indexPath.row]
            //cellEst.textLabel?.text = estudio.nome
            btnEstDrop.setTitle("\(estudio.nome!)", for: .normal)
            animate(toogle: false, type: btnEstDrop)
            }}
        
    }
    
    
}
