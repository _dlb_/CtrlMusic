//
//  BtRadio.swift
//  
//
//  Created by Leandro Brum on 20/10/2018.
//

import UIKit

@IBDesignable
public class BtRadio: UIButton {
    
    override public init (frame: CGRect){
        super.init(frame: frame)
    }
    required public init? (coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }


    @IBDesignable
    public class KGRadioButton: UIButton {
        
        interno var outerCircleLayer = CAShapeLayer ()
        interno var innerCircleLayer = CAShapeLayer ()
        
        
        @IBInspectable público var outerCircleColor: UIColor = UIColor.green {
            didSet {
                outerCircleLayer.strokeColor = outerCircleColor.cgColor
            }
        }
        @IBInspectable public var innerCircleCircleColor : UIColor = UIColor.green {
            didSet {
                setFillState ()
            }
        }
        
        @IBInspectable público var outerCircleLineWidth: CGFloat = 3.0 {
            didSet {
                setCircleLayouts ()
            }
        }
        @IBInspectable public var innerCircleGap: CGFloat = 3.0 {
            didSet {
                setCircleLayouts ()
            }
        }
        
        sobrescreve o init público (frame: CGRect) {
            super.init (frame: frame)
            customInitialization ()
        }
        // MARCA: Initialization
        required public init? (Codificador aDecoder : NSCoder) {
            super.init (codificador: aDecoder)
            customInitialization ()
        }
        interno var setCircleRadius: CGFloat {
            deixar width = bounds.width
            deixar height = bounds.height
            
            deixar length = width> height? altura: largura
            retorno (comprimento - outerCircleLineWidth) / 2
        }
        
        privada var setCircleFrame: CGRect {
            deixar largura = bounds.width
            deixar altura = bounds.height
            
            deixar raio = setCircleRadius
            seja x: CGFloat
            seja y: CGFloat
            
            se a largura> altura {
                y = outerCircleLineWidth / 2
                x = (largura / 2) - raio
            } else {
                x = outerCircleLineWidth / 2
                y = (altura / 2) - raio
            }
            
            deixar diâmetro = 2 * raio
            retorno CGRect (x: x, y: y, largura: diâmetro, altura: diâmetro)
        }
        
        privado var circlePath: UIBezierPath {
            retorno UIBezierPath (roundedRect: setCircleFrame, cornerRadius: setCircleRadius)
        }
        
        private var fillCirclePath: UIBezierPath {
            deixar trueGap = innerCircleGap + (outerCircleLineWidth / 2)
            retornar UIBezierPath (roundedRect: setCircleFrame.insetBy (dx: trueGap, dy: trueGap), cornerRadius: setCircleRadius)
            
        }
        
        private func customInitialization () {
            outerCircleLayer.frame = bounds
            outerCircleLayer.lineWidth = outerCircleLineWidth
            outerCircleLayer.fillColor = UIColor.clear.cgColor
            outerCircleLayer.strokeColor = outerCircleColor.cgColor
            layer.addSublayer (outerCircleLayer)
            
            innerCircleLayer.frame = limites
            innerCircleLayer.lineWidth = outerCircleLineWidth
            innerCircleLayer.fillColor = UIColor.clear.cgColor
            innerCircleLayer.strokeColor = UIColor.clear.cgColor
            layer.addSublayer (innerCircleLayer)
            
            setFillState ()
        }
        
        func privado setCircleLayouts () {
            outerCircleLayer.frame = limites
            outerCircleLayer.lineWidth = outerCircleLineWidth
            outerCircleLayer.path = circlePath. cgPath
            
            innerCircleLayer.frame = limites
            innerCircleLayer.lineWidth = outerCircleLineWidth
            innerCircleLayer.path = fillCirclePath.cgPath
        }
        
        // MARK: Customizado
        private func setFillState () {
            if self.isSelected {
                innerCircleLayer.fillColor = outerCircleColor.cgColor
            } else {
                innerCircleLayer.fillColor = UIColor.clear.cgColor
            }
        }
        // Overriden métodos.
        override public func PrepareForInterfaceBuilder () {
            customInitialization ()
        }
        sobrescreva public func layoutSubviews () {
            super.layoutSubviews ()
            setCircleLayouts ()
        }
        substitua public varSelecionado: Bool {
        didSet {
        setFillState ()
        }
        }
    }

}
