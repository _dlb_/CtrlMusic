//
//  CadastroViewController.swift
//  CrtlMusi
//
//  Created by Leandro Brum on 21/09/2018.
//  Copyright © 2018 Leandro Brum. All rights reserved.
//
import UIKit
import FirebaseAuth
import FirebaseDatabase

class CadastraEstudio: UIViewController, UIPickerViewDelegate  {
    
    
    @IBOutlet weak var nome: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var telefone: UITextField!

    

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }

    
    
    func exibirMensagem(titulo: String, mensagem: String){
        
        let alerta = UIAlertController(title: titulo, message: mensagem, preferredStyle: .alert)
        let acaoCancelar = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        alerta.addAction( acaoCancelar )
        present(alerta, animated: true, completion: nil)
        
    }
    
    
    @IBAction func criarContaEstudio(_ sender: Any) {
        
        //Recuperar dados digitados
        if let nomeR = self.nome.text {
            if let telefoneR = self.telefone.text {
                if let emailR = self.email.text {
                
                                
                                if nomeR != "" {
                                    
                                    if telefoneR != "" {
                                        
                                        
                                    /*
                                            //Criar conta no Firebase
                                            let autenticacao = Auth.auth()
                                            autenticacao.createUser(withEmail: emailR, password: senhaR, completion: { (usuario, erro) in
                                                
                                                if erro == nil {
                                                    
                                                    if usuario == nil {
                                                        self.exibirMensagem(titulo: "Erro ao autenticar", mensagem: "Problema ao realizar autenticação, tente novamente.")
                                                        
                                                    }else
 
                                                    {*/
                                                        //cruio objeto que recebe o usuario logado
                                                       // let userID = Auth.auth().currentUser!.uid
                                                        //crio uma
                                                        let database = Database.database().reference()
                                                        let estudio = database.child("estudio")
                                                        let estudioDados = ["nome": nomeR,"telefone":telefoneR,
                                                                            "email": emailR ]
                                                        
                                                        estudio.child(nomeR).setValue(estudioDados)
                                                        /*
                                                        //valido se usuario marcou músico ou produtor...
                                                        if (self.btmusico.isSelected == true && self.btprodutor.isSelected == false) {
                                                            
                                                            usuarios.child(userID).child("tipo_usuario").child("id_tipo_pessoa").setValue("1")
                                                            
                                                        }else{
                                                            
                                                            usuarios.child(userID).child("tipo_usuario").child("id_tipo_pessoa").setValue("2")
                                                            
                                                        }
                                                        
                                                        //recebo a seleção do usuario
                                                        let funcaoR = self.selectFuncao
                                                        
                                                        //gravo o que o usuario selecionou
                                                        usuarios.child(userID).child("funcao").child("id_funcao").setValue(funcaoR)
                                                        */
                                                        //redireciona para segue com tela principal
                                                        self.performSegue(withIdentifier: "estudioLoginSegue", sender: nil)
                                                        
                                                  /*  }
                                    
                                                }else{
                                                    
                                                    /*
                                                     ERROR_INVALID_EMAIL
                                                     ERROR_WEAK_PASSWORD
                                                     ERROR_EMAIL_ALREADY_IN_USE
                                                     */
                                                    
                                                    let erroR = erro! as NSError
                                                    if let codigoErro = erroR.userInfo["error_name"] {
                                                        
                                                        let erroTexto = codigoErro as! String
                                                        var mensagemErro = ""
                                                        
                                                        switch erroTexto {
                                                            
                                                        case "ERROR_INVALID_EMAIL" :
                                                            mensagemErro = "E-mail inválido, digite um e-mail válido!"
                                                            break
                                                        case "ERROR_WEAK_PASSWORD" :
                                                            mensagemErro = "Senha precisa ter no mínimo 6 caracteres, com letras e números."
                                                            break
                                                        case "ERROR_EMAIL_ALREADY_IN_USE" :
                                                            mensagemErro = "Esse e-mail já está sendo utilizado, crie sua conta com outro e-mail."
                                                            break
                                                        default:
                                                            mensagemErro = "Digite um e-mail válido."
                                                            
                                                        }
                                                        
                                                        self.exibirMensagem(titulo: "Dados inválidos", mensagem: mensagemErro)
                                                        
                                                        
                                                    }
                                                    
                                                }//*Fim validacao erro Firebase*/
                                                
                                            }) //fim da conexao que grava no banco
                                            */
                                            
                                        }else{
                                        self.exibirMensagem(titulo: "Dados incompletos.", mensagem: "O Telefone deve ser preenchido.")
                                    }
                                    
                                 }else{
                                          self.exibirMensagem(titulo: "Dados incompletos.", mensagem: "O nome deve ser preenchido.")
                                    }
                                    
                                    
                                }else{  
                                        self.exibirMensagem(titulo: "Dados incompletos.", mensagem: "O Email deve ser preenchido.")
                                        }
                                
                            }//fim da confirmação da senha
                        }//fim do emailR
                    }//fim telefone
                }//fim nome
          


