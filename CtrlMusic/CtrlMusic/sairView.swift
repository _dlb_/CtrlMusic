//
//  File.swift
//  CtrlMusic
//
//  Created by Leandro Brum on 18/10/2018.
//  Copyright © 2018 Leandro Brum. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase


class sairView: UIViewController {
    
     
    
    //função para sumir a barra superior.
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
