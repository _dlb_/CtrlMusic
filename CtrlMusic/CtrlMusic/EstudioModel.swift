//
//  EstudioModel.swift
//  CtrlMusic
//
//  Created by Leandro Brum on 09/04/19.
//  Copyright © 2019 Leandro Brum. All rights reserved.
//


class EstudioModel {
    var email: String?
    var nome: String?
    var telefone: String?
    
    init(email: String?, nome:String?,telefone:String?) {
        self.email  = email
        self.nome   = nome
        self.telefone   = telefone
    }
    
}
