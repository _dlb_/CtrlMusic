//
//  CadastroMusicaViewController.swift
//  CtrlMusic
//
//  Created by Leandro Brum on 29/10/18.
//  Copyright © 2018 Leandro Brum. All rights reserved

import UIKit
import AVFoundation

class MusicaViewController: UIViewController {

    var player = AVAudioPlayer()

    @IBAction func atualizaVolume(_ sender: Any) {
        player.volume = sliderVolume.value
    }
    
    @IBOutlet weak var sliderVolume: UISlider!
    
    
    @IBAction func plday(_ sender: Any) { player.play()
    }
    
    @IBAction func pause(_ sender: Any) { player.pause()
    }
    
    
    @IBAction func stop(_ sender: Any) { player.stop()
        player.currentTime = 0 
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.

       if let path = Bundle.main.path(forResource: "db", ofType: "mp3") {
        let url = URL(fileURLWithPath: path)
        do {
            player = try AVAudioPlayer(contentsOf: url)
            player.prepareToPlay()
            player.play()
        } catch {
            print("Erro ao executar o som!")
        }
    
    
    }

}

}
